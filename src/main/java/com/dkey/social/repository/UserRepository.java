package com.dkey.social.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dkey.social.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
	User findByActivationCode(String code);
}
