package com.dkey.social.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dkey.social.model.Message;
import com.dkey.social.model.User;

public interface MessageRepository extends JpaRepository<Message, Long> {
	List<Message> findByTag(String tag);
	List<Message> findByAuthor(User author);
}
