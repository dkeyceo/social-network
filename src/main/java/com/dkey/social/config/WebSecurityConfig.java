package com.dkey.social.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.dkey.social.service.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder(8);
	}
//	private DataSource dataSource;
	private static String usersByUsernameQuery = "select username, password, active from usr where username=?";
	private static String authoritiesByUsernameQuery = "select u.username, ur.roles from usr u inner join user_role ur on u.id = ur.user_id where u.username=?";
	/*
	@Bean
	@Override
	protected UserDetailsService userDetailsService() {
		UserDetails user = User.withDefaultPasswordEncoder()
				.username("u")
				.password("1")
				.roles("USER")
				.build();
		return new InMemoryUserDetailsManager(user);
	}
	*/
	

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/","/registration","/static/**","/activate/*","/rest/**").permitAll()
				.anyRequest().authenticated()
			.and()
				.formLogin()
				.loginPage("/login")
				.permitAll()
			.and()
				.rememberMe()
			.and()
				.logout()
				.permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.jdbcAuthentication()
//			.dataSource(dataSource)
//			.passwordEncoder(NoOpPasswordEncoder.getInstance())
//			.usersByUsernameQuery(usersByUsernameQuery)
//			.authoritiesByUsernameQuery(authoritiesByUsernameQuery);
//		auth.userDetailsService(userService)
//		.passwordEncoder(NoOpPasswordEncoder.getInstance());
		auth.userDetailsService(userService)
			.passwordEncoder(passwordEncoder);
		
		
	}	
	
}
