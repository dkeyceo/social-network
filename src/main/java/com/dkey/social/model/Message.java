package com.dkey.social.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Message {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
 	@NotBlank(message="Please fill the message")
 	@Length(max=2048, message="Message too long")
	private String text;
 	@Length(max=255, message="Tag value too long")
	private String tag;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User author;
	
	private String filename;
	

	public Message(String text, String tag) {
		this.text = text;
		this.tag = tag;
	}
	public Message(String text, String tag, User user) {
		this.text = text;
		this.tag = tag;
		this.author = user;
	}
	public String getAuthorName() {
		return author!=null ? author.getUsername() : "<none>";
	}
}
